package net.devrob.reservamostest.repository

import net.devrob.reservamostest.api.ApiService

class Repository(private val apiService: ApiService) {
    suspend fun getPlaces(q: String?, from: String?) = apiService.getPlaces(q, from)
}