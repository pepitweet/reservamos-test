package net.devrob.reservamostest.search.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import net.devrob.reservamostest.R
import net.devrob.reservamostest.databinding.ActivitySearchBinding
import net.devrob.reservamostest.models.Place
import net.devrob.reservamostest.search.view.adapter.PlaceAdapter
import net.devrob.reservamostest.search.viewmodel.SearchViewModel
import javax.inject.Inject


const val REQUEST_ORIGIN = 2000
const val REQUEST_DESTINATION = 3000
@AndroidEntryPoint
class SearchActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySearchBinding
    private val searchViewModel: SearchViewModel by viewModels()

    @Inject
    lateinit var placesAdapter: PlaceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        binding.viewmodel = searchViewModel
        setContentView(binding.root)
        setUpUI()
        setUpArguments()
        setUpObservers()
    }

    private fun setUpArguments() {
        when (intent.getIntExtra(ARG_TYPE_SEARCH, TYPE_SEARCH_ORIGIN)) {
            TYPE_SEARCH_ORIGIN -> {
                title = getString(R.string.title_origin)
            }
            TYPE_SEARCH_DESTINATION -> {
                title = getString(R.string.title_destination)
                searchViewModel.origin.postValue(intent.getSerializableExtra(ARG_ORIGIN_SELECTED) as Place)
            }
        }
    }

    private fun setUpUI() {
        binding.rvCities.apply {
            layoutManager = LinearLayoutManager(this@SearchActivity)
            setHasFixedSize(true)
            adapter = PlaceAdapter()
            addItemDecoration(DividerItemDecoration(this@SearchActivity, LinearLayout.VERTICAL))
            (adapter as PlaceAdapter).setOnItemClickListener { place ->
                val resultIntent = Intent()
                resultIntent.putExtra("place", place)
                setResult(RESULT_OK, resultIntent)
                finish()
            }
        }
    }

    private fun setUpObservers() {
        searchViewModel.placeList.observe(this) { placeList ->
            placeList?.let {
                if (it.isNullOrEmpty()) {
                    binding.rvCities.visibility = View.GONE
                    binding.llNoFound.visibility = View.VISIBLE
                } else {
                    binding.rvCities.visibility = View.VISIBLE
                    binding.llNoFound.visibility = View.GONE
                    binding.rvCities.apply {
                        with(adapter as  PlaceAdapter) {
                            places = it
                            notifyDataSetChanged()
                        }
                    }
                }
            }
        }

        searchViewModel.searchText.observe(this) {
            if (it?.length ?: 0 > 2) {
                searchViewModel.searchCity(it)
            }
        }
    }

    companion object {
        const val ARG_TYPE_SEARCH = "TYPE_SEARCH"
        const val ARG_ORIGIN_SELECTED = "ORIGIN_SELECTED"
        const val TYPE_SEARCH_ORIGIN = 1
        const val TYPE_SEARCH_DESTINATION = 2
    }
}