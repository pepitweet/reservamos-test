package net.devrob.reservamostest.search.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import net.devrob.reservamostest.models.Place
import net.devrob.reservamostest.repository.Repository
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    val placeList = MutableLiveData<List<Place>?>()
    val searchText = MutableLiveData<String>()
    val origin = MutableLiveData<Place>()

    fun searchCity(city: String) {
        viewModelScope.launch {
            val places = repository.getPlaces(city, origin.value?.citySlug)
            when (places.isSuccessful) {
                true -> {
                    with(places.body().orEmpty()) {
                        placeList.postValue(
                            filter {
                                it.resultType?.compareTo("city", true) == 0
                            }
                        )
                    }
                }
                false -> Timber.e(places.message())
            }
        }
    }
}