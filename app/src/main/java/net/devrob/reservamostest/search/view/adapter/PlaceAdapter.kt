package net.devrob.reservamostest.search.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.devrob.reservamostest.databinding.ContentCityBinding
import net.devrob.reservamostest.models.Place
import javax.inject.Inject

class PlaceAdapter @Inject constructor() : RecyclerView.Adapter<PlaceAdapter.ViewHolder>() {
    var places: List<Place> = emptyList()
    lateinit var onClickItem: (Place) -> Unit

    fun setOnItemClickListener(block: (Place) -> Unit) {
        onClickItem = block
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ContentCityBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(places[position])
        holder.itemView.setOnClickListener {
            if (::onClickItem.isInitialized) {
                onClickItem(places[position])
            }
        }
    }

    override fun getItemCount(): Int = places.size

    inner class ViewHolder(private val binding: ContentCityBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(place: Place) {
            binding.place = place
            binding.executePendingBindings()
        }
    }
}