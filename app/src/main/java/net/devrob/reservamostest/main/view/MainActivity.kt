package net.devrob.reservamostest.main.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import net.devrob.reservamostest.databinding.ActivityMainBinding
import net.devrob.reservamostest.main.viewmodel.MainViewModel
import net.devrob.reservamostest.models.Place
import net.devrob.reservamostest.search.view.REQUEST_DESTINATION
import net.devrob.reservamostest.search.view.REQUEST_ORIGIN
import net.devrob.reservamostest.search.view.SearchActivity
import net.devrob.reservamostest.search.view.SearchActivity.Companion.ARG_ORIGIN_SELECTED
import net.devrob.reservamostest.search.view.SearchActivity.Companion.ARG_TYPE_SEARCH
import net.devrob.reservamostest.search.view.SearchActivity.Companion.TYPE_SEARCH_DESTINATION
import net.devrob.reservamostest.search.view.SearchActivity.Companion.TYPE_SEARCH_ORIGIN

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.lifecycleOwner = this
        binding.viewModel = mainViewModel
        binding.activity = this
        setContentView(binding.root)
    }

    fun originOnClick(v: View) {
        val intent = Intent(this, SearchActivity::class.java)
        intent.putExtra(ARG_TYPE_SEARCH, TYPE_SEARCH_ORIGIN)
        startActivityForResult(intent, REQUEST_ORIGIN)
    }

    fun destinationOnClick(v: View) {
        val intent = Intent(this, SearchActivity::class.java)
        intent.putExtra(ARG_TYPE_SEARCH, TYPE_SEARCH_DESTINATION)
        intent.putExtra(ARG_ORIGIN_SELECTED, mainViewModel.origin.value)
        startActivityForResult(intent, REQUEST_DESTINATION)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                REQUEST_ORIGIN -> {
                    mainViewModel.origin.postValue(data?.getSerializableExtra("place") as Place)
                    mainViewModel.destination.postValue(null)
                }
                REQUEST_DESTINATION -> {
                    mainViewModel.destination.postValue(data?.getSerializableExtra("place") as Place)
                }
            }
        }
    }
}