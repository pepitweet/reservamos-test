package net.devrob.reservamostest.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import net.devrob.reservamostest.models.Place
import net.devrob.reservamostest.repository.Repository
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    val placeList = MutableLiveData<List<Place>?>()
    val origin = MutableLiveData<Place>()
    val destination = MutableLiveData<Place>()

}