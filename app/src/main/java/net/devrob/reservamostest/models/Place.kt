package net.devrob.reservamostest.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Place(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("city_slug")
    val citySlug: String?,
    @SerializedName("display")
    val display: String?,
    @SerializedName("ascii_display")
    val asciiDisplay: String?,
    @SerializedName("city_name")
    val cityName: String?,
    @SerializedName("city_ascii_name")
    val cityAsciiName: String?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("country")
    val country: String?,
    @SerializedName("lat")
    val latitude: String?,
    @SerializedName("long")
    val longitude: String?,
    @SerializedName("result_type")
    val resultType: String?,
    @SerializedName("popularity")
    val popularity: String?,
    @SerializedName("sort_criteria")
    val sortCriteria: Double?
) : Serializable
