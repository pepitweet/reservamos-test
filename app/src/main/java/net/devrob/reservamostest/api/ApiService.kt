package net.devrob.reservamostest.api

import net.devrob.reservamostest.models.Place
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

private const val GET_PLACES = "v2/places"

private const val QUERY_Q = "q"
private const val QUERY_FROM = "from"
interface ApiService {
    @GET(GET_PLACES)
    suspend fun getPlaces(
        @Query(QUERY_Q) q: String?,
        @Query(QUERY_FROM) from: String?
    ): Response<ArrayList<Place>>
}